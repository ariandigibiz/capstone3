import Container from 'react-bootstrap/Container'
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';


import AppNavbar from './components/AppNavbar'
import FeaturedProducts from './components/FeaturedProducts'

import Register from './pages/Register'
import Login from './pages/Login';
import Home from './pages/Home'

import './App.css';



function App () {

  return (

<Router>
      <div>

        <Container fluid>

        <AppNavbar />

        <Routes>

            <Route path="/" element={<Home/>} />            
            <Route path="/register" element={<Register/>} />
            <Route path="/login" element={<Login/>} />
            
        </Routes>

      </Container>

        <>
        <div className="text-center">
        <h1>Hello, React!</h1>
        </div>
        </>

        <>
        <div className="d-flex justify-content-center">
        <FeaturedProducts />
        </div>
        </>

      </div>

     </Router> 

    );
}



export default App;