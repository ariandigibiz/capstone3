import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link, NavLink } from 'react-router-dom';



function AppNavbar() {
  return (
    <Navbar expand="lg" className="custom-navbar">
  <Container fluid> 
    <Navbar.Brand></Navbar.Brand>
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mx-auto">
        <Nav.Link as={ Link } to="/" style={{ color: 'white' }}>Home</Nav.Link>
        <Nav.Link as={ Link } to="/products" style={{ color: 'white' }}>Products</Nav.Link>
        <Nav.Link as={ Link } to="/register" style={{ color: 'white' }}>Register</Nav.Link>
        <Nav.Link as={ Link } to="/login" style={{ color: 'white' }}>Login</Nav.Link>
      </Nav>
    </Navbar.Collapse>
  </Container>
</Navbar>

  );
}


export default AppNavbar;